### Create a merge request to add security scans to our pipeline

### Theme

This section focuses on shifting left as a security practice and how your code changes will display security results after a commit rather than months down the line

# Step 1: Adding Security Scans

1. First make sure that you are on the main page of the project you just forked in. It is best if you have these instructions open in a separate tab/screen while completing the tasks.
  
2. Once ready use the left hand navigation menu to click through **Build \> Pipeline editor**. Here you will see the current set up of our main branch pipeline. Notice that there are two stages, which are further defined below.
  
3. This pipeline does very little in terms of security scanning and only has a simple unit test defined currently. Lets go ahead and create a new branch to add out changes. Use the left hand navigation menu to click through **Code \> Branches** then click **New branch**. Name the branch **_secure-pipeline_** and make sure it is based off of **_main_**, then click **Create Branch**.
  
4. Once again use the left hand navigation menu to click through **Build \> Pipeline editor** to get back to the editor page. Then in the top left of the editor view you can click the branch dropdown to then select **_secure-pipeline_**. We then want to change the pipeline yaml to be the code below:

```plaintext
image: docker:latest

services:
  - docker:dind

variables:
  CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
  DOCKER_DRIVER: overlay2
  ROLLOUT_RESOURCE_TYPE: deployment
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
  DAST_BAS_DISABLED: "true"
  CI_DEBUG_TRACE: "true"
  # CI_PROJECT_PATH_SLUG: "tanukiracing"
  SECRET_DETECTION_EXCLUDED_PATHS: "Courses"
  

stages:
  - build
  - test

include:
  - template: Jobs/Test.gitlab-ci.yml
  - template: Jobs/Container-Scanning.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/SAST-IaC.gitlab-ci.yml

build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE

sast:
  variables:
      SEARCH_MAX_DEPTH: 12
```

5. First looking at the **_include_** section you can see that a number of security templates have been brought into our project. These define different scans and jobs that will now be ran based off of our **_stages_**. To get a better look into the templates you can click **Full configuration** which will show the true pipeline yaml with all of the templates brought in. You can also click the branch icon in the top left to then click into a specific template to get its definition.

6. Click **Edit** again to be brought back to our normal editor. Notice that we have defined an additional job to override one of the SAST jobs we have brought in through the **_Jobs/SAST.gitlab-ci.yml_** template.

7. Now that our changes are in lets click **Commit changes** at the bottom of the page.

> If you run into any issues you can use the left hand navigation menu to click through **CI/CD -\> Pipelines**, click **Run pipeline**, select **_security-workshop-pipeline_** and click **Run pipeline** once again.

> [Docs for GitLab CICD](https://docs.gitlab.com/ee/ci/)

# Step 2: Ask GitLab Duo

1. What if we hadnt known where to find our running pipelines? Or we wanted to learn more about the includes keyword in the pipeline above? We can use the new GitLab Duo Chatbot to do that and more.
  
2. Click the **? Help** button in the bottom left of the screen, then click **Ask GitLab Duo.** This will open up a chat prompt on the right hand side of the screen for you to type your questions. Go ahead and type "What Security features does GitLab have?"
  
3. For the rest of the workshop you can use this chat bot to ask any questions that may come up. You can even ask it coding questions if you get stuck!
  
#  Step 3: Creating the Merge Request

1. Now to actually merge in the new pipeline we want to use the left hand navigation menu to click through **Code \> Branches** & then click **Merge request** in the secure-pipeline section. **ENSURE YOU HAVE REMOVED THE FORK RELATIONSHIP BEFORE DOING THIS**
  
2. On the resulting page scroll down to the **_Merge options_** and uncheck **Delete source branch when merge request is accepted**. You can leave the rest of the settings as is then click **Create merge request**.
  
3. First resolve any merge conflicts that may exist, but you should see a pipeline kick off. If you click the hyperlink it will bring you to the pipeline view where you can see all of the various jobs that we added to our yaml file. While this runs we are going to move forward to set up our compliance framework pipeline but we will check back in a bit to see the results of our scanners.

### Create a compliance framework that will be enforced when we merge our code into main

### Theme

We are going to be creating a compliance framework that will ensure our pipeline runs the correct jobs in the right order. This ensures no one of the dev team can skip a few steps and introduce a vulnerability

# Step 1: Defining Our Framework

1. First we are going to just take a look at the compliance framework we plan to apply to our project. In a new tab navigate [here](https://gitlab.com/gitlab-learn-labs/sample-projects/tanuki-racing-compliance-framework/-/blob/main/.compliance-gitlab-ci.yml) to see the compliance framework we intend on applying to our codebase.
  
2. First take a look at the stages section, where each stage defined must be run in order for each pipeline the framework is applied to. Note that the ***.pre*** stage starts with "." so that it dosent have to be defined to be run.
  
3. Below the stages we define a simple ***compliance_job*** that will run during the ***.pre*** stage that just prints out a message letting the user know the framework has been applied. The ***.post*** job acts similar but print an end message 
  
4. Lastly at the end of the yml the includes section allows pipelines that the framework is applied on to build off of the job definitions. Without it this framework would replace the 
.gitlab-ci.yml file.

> [Docs for Compliance Pipeline](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html#compliance-pipelines)

# Step 2: Applying the framework

1. You can now close this tab and navigate back to your **Tanuki Racing** project. Once there use the left hand navigation menu to click through **Settings > General**. Next scroll down to the ***Compliance framework*** and click the **Choose your framework** dropdown.
  
2. On the resulting drop down select **Tanuki Racing CF** then click **Save changes**.
  
3. Now if you go back to your project home screen by clicking the name of your project in the top left you can see that the framework is applied. Note that our compliance template was defined at the top of the group allowing us to apply it on any projects in the group.

### Take a look at the results of our pipeline running for the merge request, then merge the code to generate the security reports

### Theme

Following our shift left trend we will see how security results are included throughout each step of the deployment cycle.

# Step 1: Merge Request Security Results

1. Use the left hand navigation menu to click back into **Merge requests** and then click the merge request we created earlier.

2. Right below the approvals section we can see that our **_Code Quality, License Compliance, & Security scanning_** templates we included have generated full security reports unique to this branch for us to view. These reports are generated with each commit so we always know if we have introduced a new bug before its deployed out and disaster strikes.

3. Go ahead and take some time to expand each report and look through the results, then in the **_Security scanning_** section click on any of the critical/high vulnerabilities.
  
4. From this view we can see exactly where it occurred in the code & read up on why its a risk. After taking a look at this we can click the comment icon at the bottom to state that we checked it out and are not worried, then click **Add comment & dismiss**.

5. Next we will want to scroll down and merge our code. At the bottom of the request click **Merge** to kick off our pipeline.
  
6. Once merged use the left hand navigation menu to click through **Build \> Pipelines** and click into the most recently kicked off pipeline. At this point you will go on a quick break until the pipeline completes.

# Step 2: Parsing the Results

1. Now that your **_main_** pipeline has completed the reports under **_Security & Compliance_** have been generated. These reports will only be generated if you run a pipeline against main.
  
2. Use the left hand navigation menu to click through **Secure-\> Security Dashboard**. This will bring up a dashboard view of all of the security vulnerabilities & their counts over time so you can track your work as you secure your project. This dashboard takes a long time to collect data so if yours still has no results your presenter will show you the dashboard of a deployed Tanuki Racing application [here](https://gitlab.com/gitlab-learn-labs/webinars/tanuki-racing/tanuki-racing-application/-/security/dashboard)
  
3. We have already seen how to view the vulnerabilities in the pipeline view, but now lets use the left hand navigation menu and click through **Secure -\> Vulnerability Report** to view the full report
  
4. First change the _All tools_ section under **Tools** to just filter on SAST. We can then click into any of the SAST vulnerabilities shown.
  
5. Inside the vulnerability we will want to click the _Explain vulnerability_ button within the **Explain this vulnerability and how to mitigate it with AI** section. This will result in a popup appearing on the right hand side with some information on what the vulnerability is and how you can fix it. The Explain This Vulnerability feature currently works on any SAST vulnerabilities.

# Step 3: Preventive Security Policies

1. We next want to change **Tool** to _Secret Detection_. Notice that we havent leaked any secure tokens and want to keep it that way in the future as it happens far too often.
  
2. To prevent this from ever happening in the future we can set up a new policy to run on all future merge requests. For our use case leaked tokens are easy mistakes that can lead to massive problems so we will create a quick policy to stop that. Use the left hand navigation menu to click through **Secure \> Policies** and then click **New policy**. On the resulting page click **Select policy** under **_Scan result policy_**.
  
3. Add a name to the policy, then under the **_Rules_** section we want to select **Security Scan** in the **When** dropdown list. Then we want to change **All scanners** to be **_Secret Detection_** and **All protected branches** to **default branch**.
  
4. Then under actions choose **individual users** as the **_Choose approver type_** and add **_lfstucker_** as the required approver. Next uncheck **Prevent pushing and force pushing**.

> Please ensure you do this step or you will be blocked later on in the workshop

5. Lastly click **Configure with a merge request**. On the resulting merge request click ***merge*** and you will be brought to your new policy project that is applied to our workshop application. If you were to create another merge request with the leaked token still in the code based merging would be prevented until it was removed or you added your approval.
  
6. Before we move on lets go back to our project. Use the breadcrumbs at the top of the screen to click into your group, then once again click into your project.

> [Docs for policies](https://docs.gitlab.com/ee/user/application_security/policies/)

# Step 4: Take Action on Our Vulnerabilities

1. Now that we have a protective policy in place lets go ahead and ensure it works by removing the Secrets currently in the code base. From the main page our project lets go ahead and click **Web IDE** in the **Edit** dropdown list.
  
2. Click into the **_cf-sample-scripts/eks.yaml_** file and add our fake token **_aws_key_id AKIAIOSF0DNN7EXAMPLE_** at the end of the line 6. eg: Change the **description** from **_The name of the IAM role for the EKS service to assume._** to **The name of the IAM role for the EKS service to assume, using aws_key_id AKIAIOSF0DNN7EXAMPLE.**.
  
3. Once added click the source control button on the left hand side, add a quick commit message, then click the down arrow.
  
4. On the resulting drop down click **Yes** to open a new branch, then click the **_Enter_** key. A new popup will appear where we want to then click **Create MR**

5. Scroll to the bottom, uncheck **_Delete source branch when merge request is accepted_**, and click **Create merge request**
  
6. On the resulting MR notice that our policy requires approval from **_lfstucker_** before we are able to merge. In order for us to merge in the future we will have to remove the token and wait for the full pipeline to run.

> [Docs on automatically revoking secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/#responding-to-a-leaked-secret)

### We will take a look at the SBOM report that our scanners created as well as see the various licenses that our scanners detected

### Theme

With the uptick of major security breaches hitting headlines many governments have started to require their developers and those that work with the Government to provide detailed reports on the dependencies of their applications. This section will show you how GitLab creates these reports for you.

# Step 1: Review & Download SBOM report

1. Using the left hand navigation menu click through **Secure \> Dependency list** to view all of the dependencies that are directly and indirectly included in your application.
  
2. Click through a few of the pages and notice the components that are all directly/indirectly included in your application.
  
3. Next click **Export** to download the SBOM report in CycloneDX json format. If you then open the download you can see all of the information displayed. To learn more about CycloneDX format go [here](https://cyclonedx.org/)

> [See how you could have used GitLab to detect log4j](https://about.gitlab.com/blog/2021/12/15/use-gitlab-to-detect-vulnerabilities/)

# Step 2: License Compliance

1. Using the left hand navigation menu click through the **Secure \> License Compliance** to view all of the licenses detected in your project.
  
2. Lets say we decided we want to prevent the use of the BitTorrent Open Source License v1.1 License. Using the left hand navigation menu click through the **Secure \> Policies** then click **New policy**.
  
3. Click **Select policy** under **Scan result policy**
  
4. In the **New scan result policy form** that appears, provide the following mandatory information:
    * Name: Deny BitTorrent Open Source License v1.1 License
    * Policy status: Enabled
    * Rules: If **Select scan type** == **License Scan**, rest of first section stays as is
    * **Status is** both **Newly Detected** and **Pre-existing**
    * License is **matching** **_BitTorrent Open Source License v1.1_**
    * Actions: Require 1 approval from: **Individual users** **lfstucker**
    * Uncheck **Prevent pushing and force pushing**
    * Click **Configure with a merge request**

5. Merge the new merge request into the existing security policy project.
  
6. _Remember to go back to your project using the breadcrumb, clicking on your group, then clicking on your project._
  
7. Now if we were to run a new pipeline for a MR, a new approval rule based on this license compliance policy will be added to prevent any software using the BitTorrent Open Source License v1.1 license from being merged and the security bot will notify you that you have a policy violation.


### We will wrap up the last few security capabilities that GitLab offers beyond what is possible within our CICD configuration by adding on demand scans, audit events, and further configurations for our scans

### Theme

In case of security risks events these tools are extremely handy at quickly troubleshooting what may be the issue with your application.

# Step 1: Set Up an On Demand Scan

1. Using the left hand navigation menu click through **Secure > On-demand scans**. We then want to click **New scan**.
  
2. In the ***Scan configuration*** section add a name and description but leave the rest blank.
  
3. Next in the ***DAST configuration*** section click **Select scanner profile** then **New scanner profile**, give it a name, leave the rest as is and click **Save profile**.
  
4. We then want to click **Select site profile** followed by **New site profile**. Give it a name an under ***Target URL*** enter http://www.example.com/home. It is out of scope to fully deploy out this application therefore the DAST scanner will not run, but think about how you could customize these settings for your own application.
  
5. Then click **Save profile** and scroll down to **Save scan**.
  
6. Your presenter can also show you what this scan looks like on a live [Tanuki Racing deployment](https://gitlab.com/gitlab-learn-labs/webinars/tanuki-racing/tanuki-racing-application/-/on_demand_scans#/all)

> [Docs on DAST Scanning](https://docs.gitlab.com/ee/user/application_security/dast/)

# Step 2: Project Audit Events

1. Using the left hand navigation menu click through **Secure > Audit events** to get the report on all actions taken on the project for the past month. If no events are shown you may need to edit the time frame.
  
2. You should be able to see some of the actions you have taken and where they occurred in the project hierarchy

# Step 3: Extra Scanner Configuration

1. Using the left hand navigation menu click through **Secure > Security configuration** to see all of the scans we have enabled. For this exercise we want to click **Configure SAST**
  
2. Take note of all of the various settings we can change about the scan to better suit our needs. 
  
3. Most of these settings can also be changed by adding variables to your pipeline, but often it is easier to change from this view.
  
4. Still within the ***Security Configuration*** settings we want to click the **Vulnerability Management** tab and enable the security training providers. Now our Vulnerability will link out to certain security trainings if they are related to an existing course. **_Please note some of the training courses do require a subscription_**
  
# Step 4: CODEOWNERS

1. Another compliance feature you should take advantage of is the use of the CODEOWNERS file. To do this we will first navigate back to the main page of our project and click **Web IDE**. Once in the IDE create a new file called ***CODEOWNERS***
  
2. Add the code below to the file. **MAKE SURE YOU REPLACE GITLAB ID WITH YOUR ID**:
    ```
    [Infrastructure]
    *.yml @gitlab_id
    ```

3. Now we want to commit this code to main. Go ahead and click the **Source Control** button on the left hand side, add a commit message then click **Commit & Push**. Next on the resulting dropdown make sure you click commit to main, then on the popup click the **Go to project** button. 

4. Depending on the time your instructor may show you how to open a new mr against the ci yml file, otherwise now if you were to try and edit a file within the project ending in ***.yml*** it would require an approval from the ID you specified in your CODEOWNERS file.

> [Docs for CodeOwners](https://docs.gitlab.com/ee/user/project/code_owners.html)
